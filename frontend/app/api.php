<?php

class Api
{
    public array $cityList;
    
    public function __construct($notforTest = null)
    {
        if($notforTest == null)
            $this->cityList = $this->getCitiesData();
    }

    public function getCitiesData() : array
    {
        // $URLCity = "http://back/weatherforecast/cities";
        $URLCities = "http://back/weatherforecast/";

        $chCities = curl_init();
        curl_setopt($chCities, CURLOPT_URL,$URLCities);
        curl_setopt($chCities, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($chCities, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($chCities, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        $resultCity = curl_exec($chCities);
        $status_codeCity = curl_getinfo($chCities, CURLINFO_HTTP_CODE); //get status code
        curl_close ($chCities);

        $cityData = json_decode($resultCity);

        foreach ($cityData as $key => $value) {
            $value->display = $value->cityName." (".$value->country.")";
        }
        return $cityData;

    }

    public function getCities(){
       
        $citylist = array_column($this->cityList,"display");
    
        echo json_encode($citylist);
    }

    public function getWeather(){
        
        $username = getenv('username');
        $password = getenv('password');

        $data = json_decode(file_get_contents('php://input'), true);

        $city =$data['city'];

        $getDateTime = $data['date'];

        
        $getDateTimeExport = $this->getPrevisionDate($getDateTime);

        if($getDateTimeExport == null){
            echo "error";
            return;
        }

    
        $date2 = new DateTime ($getDateTime);
        $date2->modify('+1 day');
    
        $date2value = date_format ($date2,'Y-m-d')."T00:00:00Z";
    
    
        $cityInfo = $this->getLatlongFromCity($city);

        
        $weatherURL = 'https://api.meteomatics.com/'.$getDateTimeExport.'--'.$date2value.':PT1H/t_2m:C,precip_1h:mm,wind_speed_10m:ms/'.$cityInfo->latitude.','.$cityInfo->longitude.'/json';
    

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$weatherURL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        $result=curl_exec ($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
        curl_close ($ch);

        if($status_code !=200){
            return "error weather not found";
        }
    
        $weatherAPI = json_decode($result);
        

        $weatherAPIData = $weatherAPI->data;
    
        $temperatureArray = $weatherAPI->data[0];
        $precipitationArray = $weatherAPI->data[1];
        $windSpeedArray = $weatherAPI->data[2];
    
    
        $restHour = 24 - intval(date_format (new DateTime ($getDateTimeExport),'H'));
    
        $output = [];
    
        for ($i=0; $i < $restHour; $i++) { 
    
            $output[$i]['hour'] = date_format (new DateTime ($temperatureArray->coordinates[0]->dates[$i]->date),'H');
            $output[$i]['temp'] = $temperatureArray->coordinates[0]->dates[$i]->value;
            $output[$i]['precip'] = $precipitationArray->coordinates[0]->dates[$i]->value;
            $convertoNoeux = $windSpeedArray->coordinates[0]->dates[$i]->value * 1.9438444924574;
            $output[$i]['windSpeed'] = round($convertoNoeux, 2);
    
            
            $roundPrecip = ceil($output[$i]['precip']);
            if($roundPrecip >= 4)
                $roundPrecip = 4;
    
    
            $output[$i]['precipSound'] = $roundPrecip;
    
        }
    
        echo json_encode($output);
    }

    function getLatlongFromCity($cityname){
        $city = null;
        foreach ($this->cityList as $key => $value) {
           if($value->display == $cityname){
                $city = $value;
               break;
           }
        }
        return $city;
    }

    function getPrevisionDate($getDateTime){
        $date1 = strtotime(date(date_format (new DateTime ($getDateTime),'Y-m-d')));
        $now = strtotime(date(date_format (new DateTime ('now'),'Y-m-d')));

        if($date1 == $now){
            return date_format (new DateTime ('now'),'Y-m-d')."T".date('H').":00:00"."Z";
        }elseif ($date1 > $now){// Future
            $getDateTimeExport = date_format (new DateTime ($getDateTime),'Y-m-d')."T00:00:00Z";
            return $getDateTimeExport;
        }else if ($date1 < $now){ // Past
            return 0;
        }
    }

    
}


?>