# Sonicast

## Introduction
Sonicast est une web application de météo sonifée constituée de 3 paries:
* Le fontend est basé sur AngularJs, JQuery et PHP. Il recueille des données et les envoie à l'API météo afin de renvoyer les prévisions météo au site Web
* Le backend est en ASP .net core C# responsable à donner la liste des villes avec leur coordonnées spaciales (longitude et lattiltude).
* Une api de météo "Meteormatics" pour donner les prévisions du météo pour une ville donnée.

## I - Dockerisation de l'application
Pour garantir l'aspect évolutivité et scalabilité de l'application, la première étape et de dockeriser l'application alord il faut mettre en place :
* un container pour le frontend : une image php:7.4-apache utilisant les fichers de l'application de sonicast
* un container pour le backend : une image mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim pour les ficher C# de  l'application de sonicast

### Déploiement de l'application
#### Récupération du projet
``` bash
cd $HOME
git clone https://gitlab.com/GeoG9/sonicast.git
cd sonicast
```
#### Création de l'image "sonicast-frontend"
```console
cd frontend
docker build --no-cache -t sonicast-frontend .
```
#### Création de l'image "sonicast-backtend"
```console
cd backend
docker build --no-cache -t sonicast-backend .
```

#### Lancement de l'Application via docker-compose
```console
cd sonicast
docker-compose up
```
## II - Chaine de CI dans Gitlab
Un pipeline est un groupe de travaux qui sont exécutés par étapes (lots).Tous les travaux de l'étape sont exécutés en parallèle et, s'ils réussissent tous, le pipeline passe à l'étape suivante. si l'un des travaux échoue, l'étape suivante n'est pas exécutée.
### Création d'une pipeline
En premier lieu, après la création du ficher `.gitlab-ci.yml` dans le repertoire racine de l'application de sonicast.
Le but est de construire des images a partir du code alors on a besoin d'une seul étape dans le ficher `.gitlab-ci.yml`
```
stages:
  - build
```
L'étape de build y compris push image est automatiquement executée par le runner, qui a accès au code source du projet. Les images sont disponibles dans le  registry de gitlab.

## III - Travail personnel réalisé
Création une chaine d'intégration continue pour l'application Sonicast.
### Définition
La définition d'une chaine d'integration selon Martin Fowler :
L'intégration continue est une pratique de développement logiciel où les membres d'une équipe intègrent fréquemment leur travail, Habituellement, chaque personne s'intègre au moins quotidiennement - conduisant à plusieurs intégrations par jour. Chaque intégration est vérifiée par une construction automatisée (y compris un test) pour détecter l'intégration erreurs le plus rapidement possible.
L'intégration continue constiste à :
* L'intégralité: Tous les changements jusqu'à ce point sont combinés dans le projet
* la construiction: Le code est compilé dans un exécutable ou un package
* la testabilité: Des suites de tests automatisées sont exécutées
* l' archivage :Versionné et stocké afin qu'il puisse être distribué tel quel, si vous le souhaitez
* Déployemment: Chargé sur un système où les développeurs peuvent interagir avec lui

### Pipeline
L'analyse statique d'un projet PHP par une PIC telle que Jenkins nécessite une optimisation des règles d'exclusion des répertoires ne faisant pas partis du code développé . Vous avez quelques astuces pour accélérer l'intégration continue d'un projet PHP.
Ces utilitaires peuvent être utilisés en dehors de la PIC pour vérifier tel ou tel aspect. Il existe d'autres utilitaires d'analyse de code PHP comme  PhpCheckstyle ou Yasca (audit de sécurité). Le but de ce post n'est pas de parler des autres outils intégrables à la PIC (phpunit, selenium…), mais de l'optimisation des outils de contrôle précités qui s'avèrent très gourmands en ressources système.
Le pipeline est composé de 12 étapes :
##### 1 - Checkout
Copiage tout le projet de code à partir du contrôle de version.
##### 2 - Préparation + Composer
Installation et téléchargement des dépendances afin de créer un environnement de test.
##### 3 - PHP check Syntax
**parallel-lint** pour vérifier de la syntaxe PHP dans tous les fichiers du projet
##### 4 - Tests
**Le test unitaire** est une méthode de test de logiciel par laquelle des unités individuelles de code source - des ensembles d'un ou plusieurs modules de programme informatique.
**Le test d'acceptation** consiste à évaluer si le produit fonctionne pour l'utilisateur, correctement pour l'utilisation.
**La couverture de code** est une métrique qui peut vous aider à comprendre dans quelle mesure votre source est testée.
##### 5 - Vérification du style
**phpcs**  pour l'adhésion le code à certaines conventions de codage.
Cet outils vérifient le code source PHP d'entrée et signale tout écart par rapport à la convention de codage.
##### 6 - Lines de code
**phploc** pour  mesurer rapidement la taille et la structure d'un projet PHP 
##### 7 - Détection de copier coller
**phpcpd** pour la détection du code dupliqué (par copié / collé).
##### 8 - Détection de désordre
**phpmd** pour la détection de certains bugs, de paramètres, de méthodes ou de propriétés non utilisées , etc.
Il trouve des failles de programmation courantes telles que les variables inutilisées, les blocs catch vides, la création d'objets inutiles, etc. 
##### 9 - Métrics de logiciel
**pdepend** pour des statisques sur la qualité du code  (complexité cyclomatiques, profondeur d'héritage, nombre de méthodes surchargées…). Cet utilitaire est basé sur JDepend .
Cet outil extrait des informations, notamment sur la maintenabilité, la complexité, la cohésion…
##### 10 - Construction de l'image
Mettre en place une image de docker à partir du code testé
##### 11 - Déployemment de l'image
Envoyer l’image au Dépôts d'artefacts sur Gitlab 
##### 12 - Supression de l'image docker utilisée
Ménage : Supprimer l’image localement.

## IV - Retour personnel sur le projet
Ce projet m'a permis d'uiltiser des connaissances aquises au cours de la formation. j'ai creusé dans l'intégration continue (CI) qui consiste à automatiser l'intégration des modifications de code de plusieurs contributeurs dans un seul projet logiciel. Il s'agit d'une meilleure pratique DevOps principale, permettant aux développeurs de fusionner fréquemment les modifications de code dans un référentiel central où les builds et les tests s'exécutent ensuite. Des outils automatisés sont utilisés pour affirmer l'exactitude du nouveau code avant l'intégration. 
